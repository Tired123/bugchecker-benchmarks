class Rex {
	public void eat() {}
}

public class FieldFlow {
	public Rex rex; 

	public FieldFlow() {
		this.rex = null;
	}

	public static void newRex(FieldFlow f) {
		f.rex = new Rex();	
	}

	public static void newRex2(FieldFlow f) {
		Rex d = f.rex;
		d = new Rex();
	}

	public static void main(String[] args) {
		FieldFlow n = new FieldFlow();
		n.rex = new Rex();
		newRex(n);
		newRex2(n);
		n.rex.eat();
	}
}