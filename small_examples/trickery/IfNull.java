class Rex {
	public void id() {}
}

public class IfNull {
	public static Rex getRex() {
		return null;
	}
	public static void main(String[] args) {
		Rex rex = getRex();
		int a = 0, b = 0;
		if(rex == null){
			rex.id();
		}

	}
}