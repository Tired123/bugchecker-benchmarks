
public class ControlFlowNull {
  public void id(){return;}
  public static boolean v() {return true;}
  public static void main(String[] args) {
    ControlFlowNull a = null;
    int i = 0;
    if(a != null){
		  i = 1;
		  i++;
	    a.id();
    }
    else {
      i = 2;
      i--;
    }
  }
}
