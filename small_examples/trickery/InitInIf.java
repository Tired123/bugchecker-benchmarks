import java.util.LinkedList;
import java.util.Iterator;

class Rex {
	private Rex next;

	public Rex() {
		this.next = null;
	}

	public Rex(Rex next) {
		this.next = next;
	}

	public Rex getNext() {
		return next;
	}

	public void id() {

	}

	public boolean x(){
		return true;
	}

	public void call() {

	}
}

interface RexIterator {
	public boolean hasNext();
	public Rex next();
	public void remove();
}

class RexList implements RexIterator {
	private Rex head;
	private Rex current;

	public RexList(Rex head) {
		this.head = this.current = head;
	}
			
	public boolean hasNext() {
		return current != null;
	}

	public Rex next(){
		current = current.getNext();
		return current;

	}

	public void remove() {
        throw new UnsupportedOperationException();
    }

}

public class InitInIf {
	public static void deref(Rex a) {
		a.id();
	}

	public static Rex getNull(Rex a) throws NullPointerException{
		// deref(a);
		return a;
	}

	public static boolean bool(Rex a) throws NullPointerException{
		if(a == null){
			return false;
		}
		return true;
	}

	public static LinkedList<Rex> a() {
		return new LinkedList<Rex>();
	}


	public static void main(String[] args) {
		Rex rex = getNull(null);
		if(rex != null){
			if(bool(rex)){
				rex.id();
			}
		}

	}
}


