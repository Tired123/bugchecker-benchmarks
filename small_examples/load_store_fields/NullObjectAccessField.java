
public class NullObjectAccessField {
  int x;

  public static void main(String[] args) {
    NullObjectAccessField bob = null;
    int y = bob.x;
  }
}
