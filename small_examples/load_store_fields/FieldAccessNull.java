public class FieldAccessNull {
	public static void main(String[] args) {
		Rex rex = null;
		int a = 4;
		while(a > 0){
			rex.grade = 0;
			rex = new Rex(a);
			a--;
		}
		
	}
}

class Rex {
	public int grade;

	public Rex(int grade){
		this.grade = grade;
	}
}