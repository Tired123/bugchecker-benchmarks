public class ArrayElementNull {
	public static void main(String[] args){
		String[] array = new String[2];
		array[0] = null;
		array[1] = "a";
		int length = getFirstElement(array).length();
	}

	public static String getFirstElement(String[] array){
		return array[0];
	}
}