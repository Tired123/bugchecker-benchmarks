class Rex {
	private int grade;

	public Rex(int grade){
		this.grade = grade;
	}

	public int getGrade() { return grade; }
}

public class ArrayNullObjectVCall {
	public static void main(String[] args){
		Rex[] array = null;
		array[0].getGrade();
	}
}