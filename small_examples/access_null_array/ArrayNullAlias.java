public class ArrayNullAlias {
	private static void useArray(int[] arr1, int[] arr2){
		arr1 = arr2;
		arr1[0] = 0;
	}
	public static void main(String[] args) {
		int[] arr1 = new int[4];
		int[] arr2 = null;
		useArray(arr1, arr2);
			
	}
}